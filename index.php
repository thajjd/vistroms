<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Viströms Arbetsprov</title>
	<meta name="viewport" content="width=device-width,maximum-scale=1.0,initial-scale=1.0,minimum-scale=1.0,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html, charset=utf-8">
	<script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/slick.min.js"></script>
	<script src="js/scripts.js"></script>
	<link rel="stylesheet" href="css/project.css">
</head>
<body>
  <section class="wide-container topside-bar">
    <div class="content-container">
      <div class="left">
        <span>FRI FRAKT & FRIA RETURER</span><span>1-3 DAGAR LEVERANS</span>
      </div>
      <div class="right">
        <span>BLOGG</span><span>MITT KONTO</span><span>KUNDTJÄNST</span>
      </div>
    </div>
  </section>
  <header class="wide-container primary-menu-bar">
    <div class="content-container">
      <div class="desktop-menu">
        <nav>
          <ul>
            <li>HERR</li>
            <li>DAM</li>
            <li>MÄRKEN</li>
            <li>REA</li>
          </ul>
        </nav>
        <div class="logo">
          <img src="img/hackett.jpg" alt="Hackett Logo">

        </div>
        <div class="utils">
          <nav>
            <ul>
              <li>SÖK</li>
              <li>LISTA (2)</li>
              <li class="basket">KUNDKORG (2) <span><img src="img/cart.svg" alt="cart"></span></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="mobile-menu">
        <div class="mobile-logo">
          <img src="img/hackett.jpg" alt="Hackett Logo">
        </div>
        <div class="mobile-button"><span><img src="img/mobile-menu-icon.svg" alt="Meny"></span></div>
        <div class="icons">
          <div class="search"><img src="img/search.svg" alt="Search"></div>
          <div class="cart"><img src="img/cart.svg" alt="Cart"></div>
        </div>

        <nav class="mobile-list">
          <ul>
            <li>HERR</li>
            <li>DAM</li>
            <li>MÄRKEN</li>
            <li>REA</li>
            <li>LISTA (2)</li>
            <li>BLOGG</li>
            <li>MITT KONTO</li>
            <li>KUNDTJÄNST</li>
          </ul>
        </nav>

      </div>

    </div>
    <div class="wide-container commercial">
      <div class="content-container">
        <span>FRI FRAKT & FRIA RETURER</span><span>1-3 DAGAR LEVERANS</span>
      </div>
    </div>
  </header>
  <section class="wide-container highlight-hero">
    <div class="content-container">
      <div class="slider">
        <div class="slide" style="background-image: url('img/main.jpg');"></div>
        <div class="slide" style="background-image: url('img/main.jpg');"></div>
        <div class="slide" style="background-image: url('img/main.jpg');"></div>
      </div>
      <div class="highlighted-product-info">
        <div class="banner">NYHET</div>
        <div class="in-store"><img src="img/checkbox.svg" alt="Ikryssad checkbox"> <span>FINNS I LAGER</span></div>
        <img src="img/hackett.png" alt="Hackett Logo">
        <p>Hackett Garment Dyed Oxford Slim Fit Shirt White</p>
        <p class="price">999 kr</p>
        <div class="size">
          <span>VÄLJ STORLEK</span>
          <div class="size-box selected"><span>S</span></div>
          <div class="size-box"><span>M</span></div>
          <div class="size-box"><span>L</span></div>
        </div>
        <div class="button add-to-cart"><span>LÄGG TILL I VARUKORG</span></div>
        <p class="article-nr">ARTIKELNUMMER: <span class="number">HM3049975AL</span></p>
        <div class="accordion">
          <div class="part active">
            <div class="tab">
              <span>PRODUKTBESKRIVNING</span>
            </div>
            <div class="content">Detta är lite text som berättar om produkten</div>
          </div>
          <div class="part">
            <div class="tab">
              <span>STORLEK & PASSFORM</span>
            </div>
            <div class="content">Detta är lite text som berättar om produkten</div>
          </div>
          <div class="part">
            <div class="tab">
              <span>TVÄTT & SKÖTSELRÅD</span>
            </div>
            <div class="content">Detta är lite text som berättar om produkten</div>
          </div>
          <div class="part">
            <div class="tab">
              <span>LEVERANS & RETUR</span>
            </div>
            <div class="content">Detta är lite text som berättar om produkten</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="wide-container products ">
    <div class="content-container">
      <div class="product-links">
        <div class="left">MER FRÅN HACKETT LONDON</div>
        <div class="right">SE ALLA HACKETT PRODUKTER <span class="right-arrow"><img src="img/arrow-right.svg"></span></div>
      </div>

      <div class="product-grid">
        <div class="product">
          <div class="banner">Nyhet</div>
          <img src="img/grid1.jpg" alt="image 1">
          <div class="product-desc">
            <span class="title">HACKETT LONDON</span>
            <span class="description">Hackett Garment Dyed Oxford Slim Fit Shirt White</span>
            <span class="price">999 kr</span>
          </div>
        </div>
        <div class="product">
          <div class="banner sale">Kampanj</div>
          <img src="img/grid2.jpg" alt="image 2">
          <div class="product-desc">
            <span class="title">HACKETT LONDON</span>
            <span class="description">Hackett Garment Dyed Oxford Slim Fit Shirt White</span>
            <span class="price"><span class="strike">999 kr</span> <span class="reduced">799 kr</span></span>
          </div>
        </div>
        <div class="product">
          <img src="img/grid3.jpg" alt="image 3">
          <div class="product-desc">
            <span class="title">HACKETT LONDON</span>
            <span class="description">Hackett Garment Dyed Oxford Slim Fit Shirt White</span>
            <span class="price">999 kr</span>
          </div>
        </div>
        <div class="product">
          <img src="img/grid4.jpg" alt="image 4">
          <div class="product-desc">
            <span class="title">HACKETT LONDON</span>
            <span class="description">Hackett Garment Dyed Oxford Slim Fit Shirt White</span>
            <span class="price">999 kr</span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="wide-container newsletter">
    <div class="content-container">
      <div class="text">
        <h2>FÅ DET SENASTE</h2>
        <p>Join our mailing list to stay up to date on new arrivals, promotions and all things</p>
      </div>
      <div class="mail">
        <form class="newsletter-form">
            <input type="email" class="newsletter-input" value="" placeholder="E-POSTADRESS" />
            <input type="submit" class="newsletter-submit" value="SKICKA" />
        </form>
      </div>
    </div>
  </section>
  <section class="wide-container social">
    <div class="content-container">
      <a href="#" class="social-item">FACEBOOK</a>
      <a href="#" class="social-item">INSTAGRAM</a>
      <a href="#" class="social-item">NYHETSBREV</a>
    </div>

  </section>
  <footer class="wide-container">
    <div class="content-container">
      <div class="footer-section">
        <h3>SHOP</h3>
        <nav>
          <ul>
            <li><a href="#">Herr</a></li>
            <li><a href="#">Dam</a></li>
            <li><a href="#">Märken</a></li>
            <li><a href="#">Rea</a></li>
          </ul>
        </nav>
      </div>
      <div class="footer-section">
        <h3>KUNDSSERVICE</h3>
        <nav>
          <ul>
            <li><a href="#">Order</a></li>
            <li><a href="#">Betalning</a></li>
            <li><a href="#">Frakt & Leverans</a></li>
            <li><a href="#">Returer</a></li>
          </ul>
        </nav>
      </div>
      <div class="footer-section big">
        <h3>KONTAKT</h3>
        <div class="contact-section">
          <div class="contact-section-left">
            <ul>
              <li class="bold">KONTAKTUPPGIFTER</li>
              <li>TEL: <a href="tel:0700000000">0700 00 00 00</a></li>
              <li>E-POST: <a href="mailto:info@foretaget.se">info@foretaget.se</a></li>
            </ul>
          </div>
          <div class="contact-section-right">
            <ul>
              <li class="bold">VISTRÖM WEBB AB</li>
              <li>KUNGSGATAN 9</li>
              <li>541 35 SKÖVDE</li>
            </ul>
          </div>
        </div>
      </div>
      <hr>
    </div>
    <article class="content-container">
        <p class="seo-text">
          <span class="bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span> Saepe asperiores aut unde ad sed animi quae expedita, inventore officiis tenetur aliquam molestias neque corporis eos hic dolorem facilis odio fugit doloremque fuga maiores magnam ducimus id quod. Odit iusto illum quasi laboriosam possimus, mollitia debitis soluta minima perspiciatis accusantium neque iure aperiam similique quisquam nulla ab, vero enim.
        </p>
        <hr>
        <p class="copyright">VISTRÖM WEBB AB &copy; <?php echo date('Y'); ?>. ALLA RÄTTIGHETER RESERVERADE</p>
    </article>
  </footer>

</body>
</html>
