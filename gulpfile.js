// Deep Breaths //
//////////////////

	// Gulp
	var gulp = require('gulp');

	// Sass/CSS stuff
	var sass = require('gulp-sass');
	var prefix = require('gulp-autoprefixer');
	var minifycss = require('gulp-minify-css');

	// JavaScript
	var uglify = require('gulp-uglify');
	var eslint = require('gulp-eslint');

	// refresh
	var refresh = require('gulp-refresh');
//

	// compile all your Sass
		gulp.task('sass', function (){
			gulp.src(['./css/sass/project.scss'])
				.pipe(sass({
					includePaths: ['./css/sass'],
					outputStyle: 'expanded'
				}))
				.on('error', swallowError)
				.pipe(prefix(
					"last 1 version", "> 1%", "ie 8", "ie 7"
					))
				.pipe(minifycss())
				.pipe(gulp.dest('./css'))
				.pipe(refresh());
		});

		gulp.task('php-watch', function(){
			gulp.src('./**/**.php')
			.pipe(refresh());
		});

	// Lint JS

		gulp.task('lint', function() {
			gulp.src('./js/src/**/*.js')
				// eslint() attaches the lint output to the "eslint" property
	      // of the file object so it can be used by other modules.
	      .pipe(eslint())
	      // eslint.format() outputs the lint results to the console.
	      // Alternatively use eslint.formatEach() (see Docs).
	      .pipe(eslint.format())
				.on('error', swallowError);

		});

	// Uglify JS
		gulp.task('uglify', function(){
			gulp.src('./js/src/**/*.js')
				.pipe(uglify())
				.on('error', swallowError)
				.pipe(gulp.dest('./js'))
				.pipe(refresh());
		});

//

	gulp.task('default', function(){
		refresh.listen();
		// watch me getting Sassy
		gulp.watch("./css/sass/**/*.scss", ['sass']);
		// reload that sweet page on changes
		gulp.watch("./**/**.php", ['php-watch']);
		// make my JavaScript ugly
		gulp.watch("./js/src/**/*.js", ['lint', 'uglify']);
	});

	function swallowError (error) {

	  // If you want details of the error in the console
	  console.log(error.toString())

	  this.emit('end')
	}
