(function(){
  "use strict";
  $(function(){
    $('body').on('click', '.mobile-button', function() {
      if ($('.mobile-list').not(':animated')) {
        $('.mobile-list').toggleClass('show');
      }

    });
    initAccordions();
    var form        = '.newsletter .newsletter-form',
        className   = 'newsletter-form--active',
        email       = '.newsletter input[type="email"]',
        addEventListener = function( element, event, handler )
            {
                element.addEventListener ? element.addEventListener( event, handler ) : element.attachEvent( 'on' + event, function(){ handler.call( element ); });
            },
            forEach = function( elements, fn )
            {
                for( var i = 0; i < elements.length; i++ ) fn( elements[ i ], i );
            },
            addClass = function( element, className )
            {
                element.classList ? element.classList.add( className ) : element.className += ' ' + className;
            },
            removeClass = function( element, className )
            {
                element.classList ? element.classList.remove( className ) : element.className += element.className.replace( new RegExp( '(^|\\b)' + className.split( ' ' ).join( '|' ) + '(\\b|$)', 'gi' ), ' ' );
            };

        forEach( document.querySelectorAll( form ), function( $form )
        {
            var $email = $form.querySelectorAll( email );

            if( $email.length )
            {
                $email = $email[ 0 ];
                addEventListener( $email, 'keyup', function()
                {
                    $email.value != '' && /^([\w-\.]+@([\w-]+\.)+[\w-]{2,12})?$/.test( $email.value ) ? addClass( $form, className ) : removeClass( $form, className );
                });
            }
        });

    $('.slider').slick({
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1
    });


    function initAccordions() {
      var accordions = document.getElementsByClassName('accordion');
      for (var i = 0; i < accordions.length; i++) {
          var a = accordions[i];
          for (var j = 0; j < a.childNodes.length; j++) {
            var $part = a.childNodes[j];
            if ( undefined !== $part.className && $part.classList.contains('part') ) {
              for (var k = 0; k < $part.childNodes.length; k++) {
                var $tab = $part.childNodes[k];
                if (undefined !== $tab.className && $tab.classList.contains('tab')) {
                  $tab.addEventListener('click', function() {
                    var $parent = this.parentNode;
                    if ($parent.classList.contains('active')) {
                      $parent.classList.remove('active');
                    } else {
                      $parent.classList.add('active');
                    }
                  });
                }
              }
            }
          }
        }
      }
  });
})();
